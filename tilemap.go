package tilemap

type TileMap struct {
	width  int
	height int
	tiles  []*Tile
} //struct

func NewTileMap(width, height int) *TileMap {
	t := TileMap{
		width:  width,
		height: height,
		tiles:  make([]*Tile, 0, width&height),
	}

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			tile := &Tile{
				Pos: Point{
					X: x,
					Y: y,
				},
			}

			t.tiles = append(t.tiles, tile)
		} //for
	} //for

	return &t
} //func

func (t *TileMap) Width() int {
	return t.width
} //func

func (t *TileMap) Height() int {
	return t.height
} //func

func (t *TileMap) GetTile(x, y int) *Tile {
	if x < 0 || x > t.width {
		return nil
	} //if

	if y < 0 || y > t.height {
		return nil
	} //if

	return t.tiles[x*t.width+y]
} //func

func (t *TileMap) GetTilePos(p Point) *Tile {
	return t.GetTile(p.X, p.Y)
} //func
