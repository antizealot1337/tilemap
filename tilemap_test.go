package tilemap

import (
	"testing"
)

func TestNewTileMap(t *testing.T) {
	const (
		width  = 1
		height = 2
	)

	tm := NewTileMap(width, height)

	if expected, actual := width, tm.width; actual != expected {
		t.Errorf("expected width %d but was %d", expected, actual)
	} //if

	if expected, actual := height, tm.height; actual != expected {
		t.Errorf("expected height %d but was %d", expected, actual)
	} //if

	if tm.tiles == nil {
		t.Fatal("expected tiles to be initialized")
	} //if
} //func

func TestTileMapWidth(t *testing.T) {
	tm := TileMap{
		width: 1,
	}

	if expected, actual := tm.width, tm.Width(); actual != expected {
		t.Errorf("expected %d but was %d", expected, actual)
	} //if
} //func

func TestTileMapHeight(t *testing.T) {
	tm := TileMap{
		height: 1,
	}

	if expected, actual := tm.height, tm.Height(); actual != expected {
		t.Errorf("expected %d but was %d", expected, actual)
	} //if
} //func

func TestTileMapGetTile(t *testing.T) {
	const (
		width  = 2
		height = 2
	)

	tm := NewTileMap(width, height)

	tile := tm.GetTile(0, 1)

	if tile == nil {
		t.Fatal("tile was nil")
	} //if

	if expected, actual := 0, tile.Pos.X; actual != expected {
		t.Errorf("expected x %d but was %d", expected, actual)
	} //if

	if expected, actual := 1, tile.Pos.Y; actual != expected {
		t.Errorf("expected y %d but was %d", expected, actual)
	} //if
} //func

func TestTileMapGetTilePos(t *testing.T) {
	const (
		width  = 3
		height = 3
	)

	t.Run("Bad", func(t *testing.T) {
		tm := NewTileMap(width, height)

		tile := tm.GetTilePos(Point{100, 100})

		if tile != nil {
			t.Error("unexpected tile returned")
		} //if
	})
	t.Run("Good", func(t *testing.T) {
		tm := NewTileMap(width, height)

		tile := tm.GetTilePos(Point{0, 1})

		if tile == nil {
			t.Fatal("tile was nil")
		} //if

		if expected, actual := 0, tile.Pos.X; actual != expected {
			t.Errorf("expected x %d but was %d", expected, actual)
		} //if

		if expected, actual := 1, tile.Pos.Y; actual != expected {
			t.Errorf("expected y %d but was %d", expected, actual)
		} //if
	})
} //func

func BenchmarkTileMapGetTile(b *testing.B) {
	tilemap := NewTileMap(3, 3)

	for i := 0; i < b.N; i++ {
		_ = tilemap.GetTile(0, 0)
	}
}

func BenchmarkTileMapGetTilePos(b *testing.B) {
	tilemap := NewTileMap(3, 3)
	pos := Point{0, 0}

	for i := 0; i < b.N; i++ {
		_ = tilemap.GetTilePos(pos)
	}
}
