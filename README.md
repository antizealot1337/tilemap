# TileMap

A simple tile map implementation. This is not intended to interface with any
particular existing format of tile maps. It is simply meant to create a memory
structure representing tiles and data that can hold them.

## License
Licensed under the terms of the MIT license. Please refer to LICENSE for more
information.