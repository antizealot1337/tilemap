package tilemap

type Tile struct {
	Pos  Point
	Data interface{}
} //struct
